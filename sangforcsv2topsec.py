#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: sangforcsv2topsec
# @Author: RyneZ
# @Time: 2022-06-24 10:46

from openpyxl import load_workbook
import yaml
import os
import sys
import datetime

#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: converttool
# @Author: RyneZ
# @Time: 2022/4/24 10:02
from openpyxl import load_workbook
import yaml
import os
import sys
import datetime
import re


class Config():

    def __init__(self, filename):
        #wb = load_workbook(file_name+'.xlsx')
        self.wb = load_workbook(filename)
        self.configdict = {}
        self.__converttolist()
        self.parmdict = self.__readParmConfig()

    def dealQuotedObject(self):
        '''判断类型，'''
        hostlist=[]
        rangelist=[]
        subnetlist=[]
        objlist=[]
        for obj in self.configdict['object'][1:]:

            splitedvalue = obj[2].split(',')
            if len(splitedvalue)==1:
                print(obj)
                type=self.__judgeType(splitedvalue[0])
                if type=='host':
                    objstr=self.dealHost(obj)
                elif type=='range':
                    objstr=self.dealRange(obj)
                elif type=='subnet':
                    objstr=self.dealSubnet(obj)
                else:
                    continue
                print(objstr)
                objlist.append(objstr)
            elif len(splitedvalue)>1:
                print(obj)
                objnum=1
                hostlist = []
                for i in range(len(splitedvalue)):
                    type = self.__judgeType(splitedvalue[i])
                    if type == 'host':
                        hostlist.append(splitedvalue[i])
                        continue
                        #objstr = self.dealHost(obj)
                    elif type == 'range' :
                        objstr = self.dealRange([obj[0]+'_'+str(objnum),obj[1],splitedvalue[i]])
                        objnum=objnum+1
                    elif type == 'subnet':
                        objstr = self.dealSubnet([obj[0]+'_'+str(objnum),obj[1],splitedvalue[i]])
                        objnum = objnum + 1
                    else:
                        continue
                    print(objstr)
                    objlist.append(objstr)
                    objstr=''

                #暂不考虑host数量
                if len(hostlist)>0:
                    if objnum==1:
                        objstr = self.dealHost([obj[0], obj[1], ' '.join(hostlist)])
                    else:
                        objstr = self.dealHost([obj[0] + '_' + str(objnum), obj[1], ' '.join(hostlist)])
                        objnum=objnum+1
                    print(objstr)
                    objlist.append(objstr)
                if objnum>1:
                    memberlist=[]
                    for i in range(1,objnum):
                        memberlist.append(obj[0] + '_' + str(i))
                    groupstr='define group_address add name '+obj[0]+' member \''+' '.join(memberlist)+'\''
                    print(groupstr)
                    objlist.append(groupstr)
                #print(splitedvalue)
        return objlist

    def dealHost(self,obj):
        hostlist = []
        parmdict = self.parmdict['host']
        parms = parmdict['parm'].split(',')
        hoststr = parmdict['startwith']
        for i in range(len(parms)):
            if parms[i] == 'comment':
                if obj[i]=='' or obj=='None':
                    continue
            # 单个地址对象可以添加多个IP，在命令中需要用单引号
            if parms[i] == 'ipaddr':
                if ',' in obj[i]:
                    obj[i]=' '.join(obj[i].split(','))
                hoststr = ' '.join([hoststr, parms[i], "\'", obj[i], "\'"])
            else:
                hoststr = ' '.join([hoststr, parms[i], obj[i]])
        hostlist.append(hoststr)
        return hostlist

    def dealRange(self,obj):
        rangelist = []
        parmdict = self.parmdict['range']
        parms = parmdict['parm'].split(',')

        rangestr = parmdict['startwith']
        for i in range(len(parms)):
            if parms[i] == 'comment':
                if obj[i]=='' or obj=='None':
                    continue
            if parms[i] == 'ip1' or parms[i] == 'ip2':
                # 如果在单个IP输入框填了例如'192.168.1.1-192.168.1.254'，需要特殊处理
                if '-' in str(obj[i]):
                    iplist = obj[i].strip("\'").split('-')
                    obj[i] = iplist[0]
                    obj.append(iplist[1])
            rangestr = ' '.join([rangestr, parms[i], str(obj[i])])
        rangelist.append(rangestr)

        return rangelist

    def dealSubnet(self,obj):
        subnetlist = []
        parmdict = self.parmdict['subnet']
        parms = parmdict['parm'].split(',')

        subnetstr = parmdict['startwith']
        for i in range(len(parms)):
            # 如果在mask填了例如24这样的数字，需要特殊处理
            if parms[i] == 'comment':
                if obj[i]=='' or obj=='None':
                    continue
            if parms[i] == 'mask':
                if isinstance(obj[i], int):
                    obj[i] = self.__bit_length_to_netmask(obj[i])
            # 如果在单个IP填了例如'192.168.1.1/24'，需要特殊处理
            if parms[i] == 'ipaddr':
                if '/' in obj[i]:
                    subnet = obj[i].strip("\'").split('/')
                    obj[i] = subnet[0]
                    if '.' in subnet[1]:
                        obj[i + 1] = subnet[1]
                    else:
                        obj.append(int(subnet[1]))
            subnetstr = ' '.join([subnetstr, parms[i], str(obj[i])])
        subnetlist.append(subnetstr)

        return subnetlist

    def dealService(self):
        # tos和ng处理上不同，功能在子类中完成
        pass

    def dealPolicy(self):
        # tos和ng处理上不同，功能在子类中完成
        pass

    def __readParmConfig(self):
        file = open(os.path.join("config.yml"), encoding="UTF-8")
        configdict = yaml.load(file, Loader=yaml.FullLoader)
        try:
            configdict = configdict['cfg'][self.devicetype]
        except AttributeError:
            configdict = configdict['cfg']
        return configdict

    def __converttolist(self):
        for sheet in self.wb:
            self.configdict[sheet.title] = []
            for row in sheet:
                self.configdict[sheet.title].append([str(n.value).strip('\'') for n in row])
        # print(self.configdict)

    def __bit_length_to_netmask(self, mask_int):

        bin_array = ["1"] * mask_int + ["0"] * (32 - mask_int)
        tmpmask = [''.join(bin_array[i * 8:i * 8 + 8]) for i in range(4)]
        tmpmask = [str(int(netmask, 2)) for netmask in tmpmask]
        return '.'.join(tmpmask)

    def __judgeType(self,objstr):
        hostreg=re.compile('^(\d+\.\d+\.\d+\.\d+)$')
        rangereg=re.compile('^(\d+\.\d+\.\d+\.\d+-\d+\.\d+\.\d+\.\d+)$')
        subnetreg=re.compile('^(\d+\.\d+\.\d+\.\d+/\d+)$')
        hostmatch=hostreg.match(objstr)
        if hostmatch:
            return 'host'
        rangematch=rangereg.match(objstr)
        if rangematch:
            return 'range'
        subnetmatch=subnetreg.match(objstr)
        if subnetmatch:
            return 'subnet'

class tosConfig(Config):
    def __init__(self, filename):
        self.devicetype = 'tos'
        super().__init__(filename)

    def dealService(self):
        servicelist = []
        parmdict = self.parmdict['service']
        parms = parmdict['parm'].split(',')
        for obj in self.configdict['service'][1:]:
            servicestr = parmdict['startwith']
            for i in range(len(parms)):
                # 如果在单个IP输入框填了例如'0-65535'，需要特殊处理
                if parms[i] == 'port':
                    if '-' in str(obj[i]):
                        iplist = obj[i].strip("\'").split('-')
                        obj[i] = iplist[0]
                        obj[i + 1] = iplist[1]
                if parms[i] == 'port2' and obj[i] is None:
                    continue
                if obj[i] is None and obj[i + 1] is None:
                    if parms[i - 1] == 'protocol':
                        if obj[i - 1] == 1:
                            obj[i] = 0
                            obj[i + 1] = 18
                        if obj[i - 1] == 6 or obj[i - 1] == 17:
                            obj[i] = 0
                            obj[i + 1] = 65535
                servicestr = ' '.join([servicestr, parms[i], str(obj[i])])
            servicelist.append(servicestr)

        return servicelist

    def dealPolicy(self):
        policylist = []
        parmdict = self.parmdict['policy']
        parms = parmdict['parm'].split(',')
        for obj in self.configdict['policy'][1:]:
            policystr = parmdict['startwith']
            for i in range(len(parms)):
                # 如果在单个IP输入框填了例如'0-65535'，需要特殊处理
                if parms[i] == '' or parms[i]=='None':
                    continue
                if obj[i]=='' or obj[i]=='None':
                    continue
                if parms[i] in ['src', 'dst', 'service']:
                    if ',' in str(obj[i]):
                        iplist = obj[i].strip("\'").split(',')
                        obj[i] = "\'" + ' '.join(iplist) + "\'"
                    if ' ' in str(obj[i]):
                        iplist = obj[i].strip("\'").split()
                        obj[i] = "\'" + ' '.join(iplist) + "\'"
                if obj[i] is None:
                    continue
                for tranparm, values in parmdict['trans'].items():
                    if parms[i] == tranparm:
                        for realvalue, nowvalue in values.items():
                            if obj[i] in nowvalue.split(','):
                                obj[i] = realvalue
                policystr = ' '.join([policystr, parms[i], str(obj[i])])
            policylist.append(policystr)
        return policylist

    def printFile(self, type, foldername, List):
        if List:
            outoutFileName = foldername + '\\' + self.devicetype + '_' + type + '.cfg'
            newConfigFile = open(outoutFileName, 'wb')
            newConfigFile.write(
                "##!TOS config file header, DON'T EDIT THIS LINE MANUALLY\n".encode('gbk'))
            for line in List:
                newConfigFile.write(line.encode('gbk') + '\n'.encode('gbk'))
            newConfigFile.close()


class ngConfig(Config):
    def __init__(self, filename):
        self.devicetype = 'ng'
        super().__init__(filename)

    def dealService(self):
        servicelist = []
        servicenames=[]
        parmdict = self.parmdict['service']
        parms = parmdict['parm'].split(',')
        lastname=''
        for obj in self.configdict['service'][1:]:
            servicestr = [parmdict['startwith']]
            for i in range(len(parms)):
                # 如果在单个IP输入框填了例如'0-65535'，需要特殊处理
                if parms[i]=='name':
                    if obj[i]!='None':
                        lastname=obj[i]
                    else:
                        obj[i]=lastname
                    servicenames.append(obj[i])
                if parms[i] == 'protocol':
                    protocol,ports=obj[i].split(':')
                    if protocol=='TCP':
                        obj[i]='6'
                        obj[i + 2] = '\'' + ' '.join(ports.strip(';').split(',')) + '\''
                    elif protocol=='UDP':
                        obj[i]='17'
                        obj[i + 2] = '\'' + ' '.join(ports.strip(';').split(',')) + '\''
                    elif protocol=='ICMP':
                        obj[i]='1'
                        icmpreg=re.compile('.*type\\s(\\d+).*code\\s(\\d+).*')
                        icmpmatch=icmpreg.match(ports)
                        if icmpmatch:
                            obj[i + 2]='\'' + icmpmatch.group(1)+'-'+icmpmatch.group(2) +'\''
                        else:
                            continue
                    elif protocol=='ICMPv6':
                        obj[i]='58'
                        obj[i + 2] = '\'' + ' '.join(ports.strip(';').split(',')) + '\''
                    else:
                        print('异常协议：'+protocol)
                        break

                if parms[i] == 'comment' and obj[i] == 'None':
                    continue

                servicestr = servicestr+[parms[i],str(obj[i])]
            servicelist.append(servicestr)

        #判断是否有多个服务名称一致，如果有名称相同的则需要统一修改名称并添加到服务组中
        num=1
        for i in range(len(servicenames)):
            try:
                #如果当前行和下一行的名称一致，则添加后缀序号
                if servicenames[i]==servicenames[i+1] :
                    servicelist[i][2]=servicenames[i]+'_'+str(num)
                    num=num+1
                    continue
                #如果当前行和下一行不一致，但是和上一行一致，同样添加后缀序号
                elif servicenames[i]==servicenames[i-1]:
                    servicelist[i][2] = servicenames[i] + '_' + str(num)
                #重置后缀序号
                num=1
            except IndexError:
                print('处理完成')
        newnames=set(servicenames)
        servicegroups=[]
        for name in newnames:
            groupmember=[]
            if servicenames.count(name)>1:
                for i in range(servicenames.count(name)):
                    groupmember.append(name+'_'+str(i+1))
                groupstr='define group_service add name '+name+' member \''+' '.join(groupmember)+'\''
                servicelist.append(groupstr)
        return servicelist

    def dealPolicy(self):
        policylist = []
        parmdict = self.parmdict['policy']
        parms = parmdict['parm'].split(',')
        for obj in self.configdict['policy'][1:]:
            policystr = parmdict['startwith']
            for i in range(len(parms)):
                # 如果在单个IP输入框填了例如'0-65535'，需要特殊处理
                if parms[i] == 'None' or parms[i]=='':
                    continue
                if obj[i]=='' or obj[i]=='None':
                    continue
                if parms[i] in ['src', 'dst', 'service']:
                    if ',' in str(obj[i]):
                        iplist = obj[i].strip("\'").split(',')
                        #去除掉service中的‘预定义服务/’‘自定义服务/’字符串
                        if parms[i]=='service':
                            for i in range(len(iplist)):
                                if len(iplist[i].split('/'))>1:
                                    iplist[i]=iplist[i].split('/')[1]
                        obj[i] = "\'" + ' '.join(iplist) + "\'"
                        print(obj[i])
                    else:
                        if parms[i] == 'service':
                            if len(obj[i].split('/')) > 1:
                                obj[i] = obj[i].split('/')[1]
                    # if ' ' in str(obj[i]):
                    #     iplist = obj[i].strip("\'").split()
                    #     obj[i] = "\'" + ' '.join(iplist) + "\'"
                for tranparm, values in parmdict['trans'].items():
                    if parms[i] == tranparm:
                        for realvalue, nowvalue in values.items():
                            if obj[i] in nowvalue.split(','):
                                obj[i] = realvalue
                print(obj[i])
                policystr = ' '.join([policystr,parms[i],str(obj[i])])
            policylist.append(policystr)
        return policylist

    def printFile(self, type, foldername, List):
        if List:
            outoutFileName = foldername + '\\' + self.devicetype + '_' + type
            newConfigFile = open(outoutFileName, 'w', encoding='utf-8')
            newConfigFile.write(
                "##!NGTOS module config file header, DON\'T EDIT THESE LINES TILL @@ MANUALLY\n")
            for line in List:
                print(line)
                if isinstance(line, list):
                    newConfigFile.write(' '.join(line) + '\n')
                if isinstance(line,str):
                    newConfigFile.write(line + '\n')
            newConfigFile.close()


def mainfunc():
    #print('当前版本：0.1.0\n')
    #filename=input('输入当前目录下的xlsx文件名(不包括后缀名)：')
    #selecttype=input('\n1.tos防火墙\n2.NG防火墙\n请输入需要生成的命令类型序号,1或者2：')
    foldername = os.getcwd() + '\\' + datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    try:
        os.mkdir(foldername)
    except FileExistsError:
        pass
    try:
        # if selecttype=='1':
        #     print('正在生成tos设备配置...\n')
        #     tosconfig = tosConfig(filename+'.xlsx')
        #     for type in ['Host', 'Range', 'Subnet', 'Service', 'Policy']:
        #         # hostlist=config.dealCommands()
        #         # 动态执行类的方法
        #         func = getattr(tosconfig, 'deal' + type)
        #         commandlist = func()
        #         tosconfig.printFile(type, foldername, commandlist)
        # elif selecttype=='2':
            print('正在生成ng设备配置...\n')
            #ngconfig = ngConfig(filename+'.xlsx')
            ngconfig = ngConfig('template.xlsx')
            for type in ['QuotedObject', 'Service', 'Policy']:
            #for type in [ 'QuotedObject']:
                # hostlist=config.dealCommands()
                # 动态执行类的方法
                func = getattr(ngconfig, 'deal' + type)
                commandlist = func()
                ngconfig.printFile(type, foldername, commandlist)
    except FileNotFoundError:
        input('未找到该文件，请检查是否输入有误！')
        sys.exit(-1)
    input('输出目录为'+foldername+'\n生成的文件可以通过防火墙设备的部分导入功能完成添加，按任意键退出...')


if __name__ == '__main__':
    mainfunc()

